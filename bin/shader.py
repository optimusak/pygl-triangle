import pyglet
from pyglet.gl import GL_POINT,GL_TRIANGLES 
from pyglet.shapes import *
from pyglet.graphics import Batch
from pyglet.graphics.shader import Shader,ShaderProgram


class ShaderSrc:
    vertex_source = '''
#version 330

layout(location=0) in vec2 vertices;
layout(location=1) in vec4 colors;

out vec4 fragColor;

void main(){
    gl_Position = vec4(vertices,0.0,0.0);
    fragColor = colors;
}
    '''

    fragment_source = '''
#version 330
in vec4 fragColor;
out vec4 outColor;
void main(){
    outColor = fragColor;
}

    '''


class MainWindow(pyglet.window.Window):
    
    def __init__(self,width,height,**kwargs):
        super().__init__(width=width,height=height,**kwargs)
        vertex_shader = Shader(ShaderSrc.vertex_source,'vertex')
        fragment_shader = Shader(ShaderSrc.fragment_source,'fragment')
        self.shader = ShaderProgram(vertex_shader,fragment_shader)
        self.batch = pyglet.graphics.Batch()
        # Circle(0,0,100,color=(255,255,255,255),batch=self.batch)
        self.v_s = self.shader.vertex_list(3,GL_TRIANGLES,
                                batch=self.batch,
                                vertices=('f', (-0.5,-0.5,0.5,-0.5,0.0,0.5)),
                                colors=('f', (0.0,0.1,0.3,1.0,  0.2,0.0,0.5,1.0,   0.7,0.3,0.4,1.5))
            )
    
    def on_draw(self):
        # self.clear()
        self.v_s.draw(GL_TRIANGLES)

if __name__ == '__main__':

    window = MainWindow(width=640,height=480,caption="New window")
    pyglet.app.run()