import numpy.random as random
import pyglet
from pyglet.gl import GL_TRIANGLES
from pyglet.graphics.shader import Shader,ShaderProgram

vertex_source = """#version 150 core
    in vec2 position;
    in vec4 colors;
    uniform float time;
    out vec4 vertex_colors;

    void main()
    {

        gl_Position = vec4(position, 0.0, 1.0);
        vertex_colors = colors;
    }
"""

fragment_source = """#version 150 core
    in vec4 vertex_colors;
    out vec4 final_color;

    void main()
    {
        final_color = vertex_colors;
    }
"""


window = pyglet.window.Window(width=500,height=500)
shader_program = ShaderProgram(Shader(vertex_source,'vertex'),Shader(fragment_source,'fragment'))

m_batch = pyglet.graphics.Batch()
# shader_program.vertex_list(3,GL_TRIANGLES,
#                             batch=m_batch,
#                             position=('f',(-0.5,-0.5,0.5,-0.5,0.0,0.5)),
#                             # colors=('f',[random.random() for i in range(3*4)])
# )

shader_program.vertex_list_indexed(4,GL_TRIANGLES,
                                    indices=(0,1,2,2,3,0),
                                    batch=m_batch,
                                    position=('f',(-0.5,-0.5,0.5,-0.5,0.5,0.5,-0.5,0.5)),
                                    colors=('f',(1.0,1.0,1.0,1.0,
                                                 1.0,1.0,1.0,1.0,
                                                 1.0,1.0,1.0,1.0,
                                                 1.0,0.2,0.0,1.0)),
                                    
)
# shader_program['time'] = 1.0
@window.event
def on_draw():
    # print("Draw request")\
    window.clear()
    m_batch.draw()


# def update_time(dt):
#     shader_program['time'] += dt;

# pyglet.clock.schedule_interval(update_time,1/60)
pyglet.app.run()
