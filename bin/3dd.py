import pygame
import numpy as np
from transformers import Transformers
# np.matlib.multiply()
width = 500
height = width
theta = np.pi/90

translate = np.array([
    [1, 0,  width/2],
    [0,-1, height/2]
])

pp = np.array([
    [1,0,0,0],
    [0,1,0,0],
    [0,0,1,0],
    [0,0,1,0]
])

window = pygame.display.set_mode((width,height))
clock = pygame.time.Clock()
finalTran = Transformers.rotateX@Transformers.rotateZ@(-Transformers.rotateY@Transformers.rotateY)

exit_window = False

vertices = np.array([
    (-50,-50,-50, 1),
    ( 50,-50,-50, 1),
    ( 50, 50,-50, 1),
    (-50, 50,-50, 1),
    (-50,-50, 50, 1),
    ( 50,-50, 50, 1),
    ( 50, 50, 50, 1),
    (-50, 50, 50, 1)
],dtype='float64').transpose()
count = 8
vertices[-1] = 1
print(vertices)
path_map = [(0,1),(1,2),(2,3),(3,0),(0,4),(4,5),(5,6),(6,7),(7,4),(1,5),(2,6),(3,7)]

faces = [[[0,1,2,3],255]]

def pointStroke(point,stroke=2):
    # count = 1/point[2]
    # p = Transformers.projection.copy()/count
    # p[-1,-1] = 1
    # p
    point = translate@Transformers.projection@point
    # print(point)
    pygame.draw.circle(window,(255,255,233),point,stroke)

while not exit_window:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit_window = True
    
    window.fill((0,0,0,0))
    # pygame.draw.circle(window,(25,40,120),(width/2,height/2),100*1.41)
    vertices = finalTran@vertices

    # vertex = pp@vertices

    vertices /= vertices[-1]
    vertex = vertices.copy()
    vertex[:2] /= (200+vertex[2])/500
    t = Transformers.projection@vertex
    projection = translate@t

    for i in range(8):
        pygame.draw.circle(window,(i*25,i*25,255,0),projection[:,i],5)
    for i in range(12):
        i_x,i_y = path_map[i]
        i_x = projection[:,i_x]
        i_y = projection[:,i_y]
        pygame.draw.line(window,(0,i*4+100,0,0),i_x,i_y,1)
    for face,color in faces:
        pygame.draw.polygon(window,(color,)*3,[projection[:,i] for i in face])
    # for i in range(count):
    #     # vertices[i] = Transformers.rotateX@vertices[i]
    #     pointStroke(vertices[i],5)
    pygame.display.update()
    # vertices[-1] += 0.02
    clock.tick(60)