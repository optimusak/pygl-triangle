import numpy as np
from numpy import sin,cos,pi
PI = pi
angle = PI/120

class Transformers:
    projection = np.array([
        [1, 0,  0,  0],
        [0, 1,  0,  0],
        [0, 0,  0,  1]
    ])


    rotateX = np.array([
        [1,              0,            0,    0],
        [0,     cos(angle),  -sin(angle),    0],
        [0,     sin(angle),   cos(angle),    0],
        [0,              0,            0,    1]
    ])

    rotateY = np.array([
        [ cos(angle),    0,   sin(angle),    0],
        [          0,    1,            0,    0],
        [-sin(angle),    0,   cos(angle),    0],
        [          0,    0,            0,    1]
    ])

    rotateZ = np.array([
        [cos(angle),   -sin(angle),   0,    0],
        [sin(angle),    cos(angle),   0,    0],
        [         0,             0,   1,    0],
        [         0,             0,   0,    1]
    ])

    scale = np.array([
        [1,0,0,0],
        [0,1,0,0],
        [0,0,1,0],
        [0,0,0,1]
    ])
