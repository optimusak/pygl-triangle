import pyglet
from math import sin,radians
window = pyglet.window.Window(width=1080,height=720,caption="New pyglet window")
my_batch = pyglet.graphics.Batch()

circle = pyglet.shapes.Circle(300,300,100,color=(0,123,166),batch=my_batch)
rectangle = pyglet.shapes.Rectangle(100,100,30,50,color=(255,132,100),batch=my_batch)
square= pyglet.shapes.Rectangle(100,200,40,40,color=(231,222,100),batch=my_batch)
circle._angle = 0
square.anchor_position = (square.width/2,square.height/2)
@window.event
def on_draw():
    window.clear()
    my_batch.draw()

def update(dt):
    square.rotation += 1
    square.rotation %= 360
    circle._angle += radians(1)
    circle.radius += sin(circle._angle)
    

pyglet.clock.schedule_interval(update,1/60)




pyglet.app.run()