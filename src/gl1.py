import OpenGL.GL as gl
from triangle import Triangle
import OpenGL.GL.shaders as glShader
import pygame as pg
import numpy as np
import ctypes

class App:
    def __init__(self,
        name: str,
        width: int,
        height: int
    ):
        pg.init()
        self.window = pg.display.set_mode((width,height),pg.OPENGL|pg.DOUBLEBUF)
        self.clock = pg.time.Clock()
        gl.glClearColor(0.0,0.07,0.0,1.0)
        self.shader = self.create_shader('shaders/vertex.txt','shaders/fragment.frag')
        gl.glUseProgram(self.shader)
        self.triangle = Triangle()
    
    def mainloop(self):
        running = True
        while(running):
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    running = False

            gl.glClear(gl.GL_COLOR_BUFFER_BIT)
            gl.glUseProgram(self.shader)
            gl.glBindVertexArray(self.triangle.vao)
            gl.glDrawArrays(gl.GL_TRIANGLES,0,self.triangle.vertex_count)
            pg.display.flip()
            self.clock.tick(60)

        self.quit()
    
    def quit(self):
        pg.quit()
        self.triangle.destroy()
        gl.glDeleteProgram(self.shader)

    def create_shader(self,vertexFilePath,fragmentFilePath) -> gl.GLuint:
        
        with open(vertexFilePath) as f:
            vertex_src = f.readlines()

        with open(fragmentFilePath) as f:
            fragment_src = f.readlines()
        
        shader = glShader.compileProgram(
            glShader.compileShader(vertex_src,gl.GL_VERTEX_SHADER),
            glShader.compileShader(fragment_src,gl.GL_FRAGMENT_SHADER)
        )
        return shader

if __name__ == '__main__':
    app = App('first window',640,480)
    app.mainloop()
        